# EE
Name comes from **E**rnesto **E**dit. And that is all there is to it at the
moment :).

## Building from Source
We are using Custom Build System written entirely in `C` called `nob`.
It is similar to [nobuild](https://github.com/tsoding/nobuild) in spirit.
[nob.h](./nob.h) is the Build System and [nob.c](./nob.c) is the program that builds MatEdit.

Before using `nob` you need to bootstrap it. Just compile it with the available `C` compiler.
You only need to boostrap it once. After the bootstrap you can just keep running the same executable over and over again.
It even tries to rebuild itself if you modify [nob.c](./nob.c).
> **_NOTE:_** The automatic rebuild of `nob` may fail sometimes. In that case you will need to reboostrap it.

### Linux
Boostrap `nob`
```sh
$ cc -o nob nob.c
```
Compile and run the MatEdit
```sh
$ ./nob
$ ./build/matedit
```

### Windows MSCV
From within `vcvarsall.bat`. First boostrap `nob`
```
> cl.exe nob.c
```
Compile and run MatEdit
```
> nob.exe
> build\matedit.exe
```

### Cross Compilation from Linux to Windows using MinGW-w64
Boostrap `nob`
```sh
$ cc -o nob nob.c
```
Configure `nob` for cross compilation
```sh
$ ./nob config --target win64-mingw
```
Compile and run MatEdit
```sh
$ ./nob
$ wine ./build/matedit.exe
```
