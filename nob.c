#define NOB_IMPLEMENTATION
#include "nob.h"

bool
build(void)
{
	/* We only support Linux ATM */
	Nob_Cmd cmd = {0};
	nob_cmd_append(&cmd, "cc");
	nob_cmd_append(&cmd, "-std=c99", "-Wall", "-Wextra", "-pedantic");
	nob_cmd_append(&cmd, "-o", "./build/ee");
	nob_cmd_append(&cmd, "ee.c");
	if (!nob_cmd_run_sync(cmd))
		return false;
	return true;
}

int
main(int argc, char *argv[])
{
	NOB_GO_REBUILD_URSELF(argc, argv);

	if (!nob_mkdir_if_not_exists("build")) return 1;
	build();

	return 0;
}
