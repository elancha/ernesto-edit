#include <termios.h>
#include <unistd.h>

struct termios original_mode;

void
enable_raw_mode(void)
{
	tcgetattr(STDIN_FILENO, &original_mode);
	/* TODO: Should we do this? */
	/* atexit(restore_previous_mode); */
	struct termios raw_mode = original_mode;

	/* Turn off ECHO */
	raw_mode.c_lflag &= ~ECHO;
	/* Turn off canonical mode */
	raw_mode.c_lflag &= ~ICANON;

	tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw_mode);
}

void
restore_previous_mode()
{
	tcsetattr(STDIN_FILENO, TCSAFLUSH, &original_mode);
}

int
main(void)
{
	enable_raw_mode();

	char c;
	while (read(STDIN_FILENO, &c, 1) == 1 && c != 'q');

	restore_previous_mode();

	return 0;
}
